require('./libraries')

import { Vue } from './vue'
import router from './router'
import store from './../store'

Vue.material.registerTheme('default', {
  primary: 'blue',
  accent: 'red',
  warn: 'red',
  background: {
    color: 'white',
    hue: 900,
    textColor: 'black'
  }
});

export { Vue, router, store }

