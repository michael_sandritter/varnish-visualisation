window.$ = window.jQuery = require('jquery')
window._ = require('lodash')
require('bootstrap-sass/assets/javascripts/bootstrap.js')
require('vue-material')

window.demoData = {
    "schema_version": 1,
    "requests": [
      {
        "id": 294961,
        "start": "192.168.56.101 12345",
        "url": "/meincongstar",
        "protocol": "HTTP/1.0",
        "header": [
          "Connection: keep-alive",
          "User-Agent: Mozilla"
        ],
        "method": "GET",
        "subroutine-sorting": [
          "RECV",
          "HASH",
          "MISS",
          "BACKEND_FETCH"
        ],
        "subroutines": [
          {
            "name": "RECV",
            "request": {
              "header": [
                "X-Forwareded-For: test.de"
              ],
              "actions": {
                "unset": "X-Forwareded-For: test.de"
              }
            }
          },
          {
            "name": "PIPE",
            "request": {

            },
            "backend_request": {

            }
          },
          {
            "name": "PASS",
            "request": {
              "header": [
                "x-restart: 1"
              ],
              "url": "/meincongstar/"
            }
          },
          {
            "name": "PURGE",
            "request": {

            }
          },
          {
            "name": "SYNTH",
            "request": {
              "header": [

              ]
            },
            "response": {
              "reason": [
                "Moved"
              ],
              "header": [
                "X-Cacheable: NO:TTL zero"
              ]
            }
          },
          {
            "name": "HASH",
            "request": {

            },
            "properties": {
              "hit": 131112
            }
          },
          {
            "name": "HIT",
            "request": {
              "header": "X-Cache-Lifecycle-Status: healthy with positive ttl."
            },
            "requested_object": {

            }
          },
          {
            "name": "MISS",
            "request": {

            },
            "properties": {
              "link": 294940
            }
          },
          {
            "name": "DELIVER",
            "request": {

            },
            "requested_object": {

            },
            "response": {
              "header": [
                "X-Cache-Hits: 0",
                "X-Cache: MISS"
              ],
              "actions": {
                "unset": "Set-Cookie: cookies_allowed=1; path=/"
              }
            }
          },
          {
            "name": "BACKEND_FETCH",
            "backend_request": {
              "protocol": "HTTP/1.1",
              "url": "typo3/index.php?id=123435",
              "header": [
                "X-Forwarded-Proto: https",
                "X-Forwarded-For: test"
              ]
            },
            "properties": {
              "backend": "127.0.0.1 8080"
            }
          },
          {
            "name": "BACKEND_RESPONSE",
            "backend_request": {

            },
            "backend_response": {
              "reason": "OK",
              "status": 200,
              "header": [
                "Cache-Control: max-age=604800, public",
                "Strict-Transport-Security: max-age=31536000"
              ]
            },
            "properties": {
              "ttl": "VCL -1 86400 0 1498121264"
            }
          },
          {
            "name": "BACKEND_ERROR",
            "backend_response": {

            }
          }
        ]
      },
      {
        "id": 294967,
        "start": "192.168.56.101 12345",
        "url": "/api/data/clients/1/plans/316",
        "protocol": "HTTP/1.0",
        "header": [
          "Connection: keep-alive",
          "User-Agent: Mozilla"
        ],
        "method": "GET",
        "subroutine-sorting": [
          "RECV",
          "HASH",
          "MISS",
          "BACKEND_FETCH"
        ],
        "subroutines": [
          {
            "name": "RECV",
            "request": {
              "header": [
                "X-Forwareded-For: test.de"
              ],
              "actions": {
                "unset": "X-Forwareded-For: test.de"
              }
            }
          },
          {
            "name": "PIPE",
            "request": {

            },
            "backend_request": {

            }
          },
          {
            "name": "PASS",
            "request": {
              "header": [
                "x-restart: 1"
              ],
              "url": "/meincongstar/"
            }
          },
          {
            "name": "PURGE",
            "request": {

            }
          },
          {
            "name": "SYNTH",
            "request": {
              "header": [

              ]
            },
            "response": {
              "reason": [
                "Moved"
              ],
              "header": [
                "X-Cacheable: NO:TTL zero"
              ]
            }
          },
          {
            "name": "HASH",
            "request": {

            },
            "properties": {
              "hit": 131112
            }
          },
          {
            "name": "HIT",
            "request": {
              "header": "X-Cache-Lifecycle-Status: healthy with positive ttl."
            },
            "requested_object": {

            }
          },
          {
            "name": "BACKEND_FETCH",
            "backend_request": {
              "protocol": "HTTP/1.1",
              "url": "typo3/index.php?id=123435",
              "header": [
                "X-Forwarded-Proto: https",
                "X-Forwarded-For: test"
              ]
            },
            "properties": {
              "backend": "127.0.0.1 8080"
            }
          }
        ]
      },
      {
        "id": 294964,
        "start": "192.168.56.101 12345",
        "url": "/api/data/clients/1/devices",
        "protocol": "HTTP/1.0",
        "header": [
          "Connection: keep-alive",
          "User-Agent: Mozilla"
        ],
        "method": "GET",
        "subroutine-sorting": [
          "RECV",
          "HASH",
          "MISS",
          "BACKEND_FETCH"
        ],
        "subroutines": [
          {
            "name": "BACKEND_RESPONSE",
            "backend_request": {

            },
            "backend_response": {
              "reason": "OK",
              "status": 200,
              "header": [
                "Cache-Control: max-age=604800, public",
                "Strict-Transport-Security: max-age=31536000"
              ]
            },
            "properties": {
              "ttl": "VCL -1 86400 0 1498121264"
            }
          },
          {
            "name": "BACKEND_ERROR",
            "backend_response": {

            }
          }
        ]
      }
    ]
  };

